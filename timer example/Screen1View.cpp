#include <gui/screen1_screen/Screen1View.hpp>
#include <main.h>

Screen1View::Screen1View()
{

}

void Screen1View::setupScreen()
{
    Screen1ViewBase::setupScreen();
}

void Screen1View::tearDownScreen()
{
    Screen1ViewBase::tearDownScreen();
}

void Screen1View::sendTime(int time)
{
	int minutes = time / 60;
	int sec = time - (minutes * 60);
	Unicode::snprintf(TextAreaHodinyBuffer2, TEXTAREAHODINYBUFFER2_SIZE, "%02d", sec);
	Unicode::snprintf(TextAreaHodinyBuffer1, TEXTAREAHODINYBUFFER1_SIZE, "%02d", minutes);
	TextAreaHodiny.invalidate();
}

void Screen1View::stopTimer()
{
	Stop();
}

void Screen1View::startTimer()
{
	Start();
}

void Screen1View::IncrementTimerSec()
{
	IncrementTimerSecM();
}
void Screen1View::IncrementTimerMin()
{
	IncrementTimerMinM();
}
void Screen1View::DecrementTimerSec()
{
	DecrementTimerSecM();
}
void Screen1View::DecrementTimerMin()
{
	DecrementTimerMinM();
}

void Screen1View::RestartTimer()
{
	RestTimer();
}

void Screen1View::showScreen(int screen){

    FrontendApplication* App;
    App = static_cast<FrontendApplication*>(Application::getInstance());
    App->gotoScreen2ScreenSlideTransitionEast();

}




