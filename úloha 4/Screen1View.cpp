#include <gui/screen1_screen/Screen1View.hpp>
#include <main.h>

Screen1View::Screen1View()
{

}

void Screen1View::setupScreen()
{
    Screen1ViewBase::setupScreen();
}

void Screen1View::tearDownScreen()
{
    Screen1ViewBase::tearDownScreen();
}

void Screen1View::show(char *name, char *text)
{
	Unicode::strncpy(LabelNameBuffer, name, LABELNAME_SIZE);
	LabelName.setWildcard(LabelNameBuffer);
	LabelName.invalidate();

	Unicode::strncpy(text_1Buffer, text, TEXT_1_SIZE);
	text_1.setWildcard(text_1Buffer);
	text_1.invalidate();
}

void Screen1View::setFmAm(){
	changeThread(0);
}

void Screen1View::setMedia(){
	changeThread(1);
}

void Screen1View::telFun(){
	changeThread(2);
}

void Screen1View::pickUpCall(){
	pickUpOrCancelCall();
}

void Screen1View::OnRadio(){
	flexButton1.setVisible(false);
	flexButton1.invalidate();
	radioOn();
}

void Screen1View::offRadio(){
	if(permiToOff()){
		flexButton1.setVisible(true);
		flexButton1.invalidate();
		radioOff();
	}
}

